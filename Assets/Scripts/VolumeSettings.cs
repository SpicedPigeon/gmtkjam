using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class VolumeSettings : MonoBehaviour
    {
        [SerializeField]
        private AudioMixer _audioMixer;

        [SerializeField]
        private Slider _slider;
        
        private void Start()
        {
            _audioMixer.GetFloat("Volume", out float volume);
            
            float sliderValue = (volume + 30) / 30;

            _slider.value = sliderValue;
        }

        public void ValueChanged()
        {
            float volume = -30 + 30 * _slider.value;
            _audioMixer.SetFloat("Volume", volume);
        }
    }
}