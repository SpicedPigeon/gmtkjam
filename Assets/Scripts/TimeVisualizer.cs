using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class TimeVisualizer : MonoBehaviour
    {
        public TMP_Text _text;

        private void Update()
        {
            int minutes = (int)(Time.timeSinceLevelLoad / 60);
            int seconds = (int)(Time.timeSinceLevelLoad % 60);
            
            _text.text = minutes + ":" + (seconds >= 10 ? seconds.ToString() : ("0" + seconds));
        }
    }
}