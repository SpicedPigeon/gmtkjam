using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DefaultNamespace.Health
{
    public class EnemyHealth : MonoBehaviour
    {
        [SerializeField]
        private int _startHealth;

        private int _currentHealth;

        [SerializeField]
        private RectTransform _rectTransform;

        [SerializeField]
        private Animator _animator;

        [SerializeField]
        private TMP_Text _damageText;
        
        private void Awake()
        {
            _currentHealth = _startHealth;
        }

        public void Damage(int amount)
        {
            _currentHealth -= amount;

            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, 140 + 570 * ((float) _currentHealth / _startHealth));
            
            _damageText.SetText("-" + amount);
            
            _animator.SetTrigger("DamageReceived");
            
            if (_currentHealth <= 0)
            {
                PlayerPrefs.SetInt("WON", 1);
                PlayerPrefs.SetString("TIME", FindObjectOfType<TimeVisualizer>()._text.text);
                PlayerPrefs.Save();
                SceneManager.LoadScene(2);
            }
        }
    }
}