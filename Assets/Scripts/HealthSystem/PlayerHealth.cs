using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace DefaultNamespace.Health
{
    public class PlayerHealth : MonoBehaviour
    {
        [SerializeField]
        private int _startHealth;

        private int _currentHealth;

        [SerializeField]
        private RectTransform _rectTransform;

        [NonSerialized]
        public int AmountOfDamageReduction;

        public int DamagedLastBeat = 0;
        
        [Inject]
        private SignalBus _signalBus;
        
        private int _hpBefore;
        
        private void Start()
        {
            _currentHealth = _startHealth;
            
            _signalBus.Subscribe<ActionSignal>(OnBeat);
            _signalBus.Subscribe<PostActionSignal>(PostBeat);
        }
        
        private void OnBeat()
        {
            _hpBefore = _currentHealth;
        }
        private void PostBeat()
        {
            DamagedLastBeat = _hpBefore - _currentHealth;
        }
        
        public void Damage(int amount)
        {
            AmountOfDamageReduction = 0;
            
            _signalBus.Fire(new BeforeDamageReceivedSignal()
            {
                Amount = amount,
                PlayerHealth = this
            });

            amount -= AmountOfDamageReduction;

            if (amount == 0)
            {
                return;
            }
            
            _currentHealth -= amount;
            
            _rectTransform.sizeDelta = new Vector2(_rectTransform.sizeDelta.x, 140 + 570 * ((float) _currentHealth / _startHealth));

            if (_currentHealth <= 0)
            {
                PlayerPrefs.SetInt("WON", 0);
                PlayerPrefs.SetString("TIME", FindObjectOfType<TimeVisualizer>()._text.text);
                PlayerPrefs.Save();
                SceneManager.LoadScene(2);
            }
            
            _signalBus.Fire(new AfterDamageReceivedSignal()
            {
                Amount = amount,
                PlayerHealth = this
            });
        }
    }

    public class BeforeDamageReceivedSignal
    {
        public int Amount;
        public PlayerHealth PlayerHealth;
    }

    public class AfterDamageReceivedSignal
    {
        public int Amount;
        public PlayerHealth PlayerHealth;
    }
}