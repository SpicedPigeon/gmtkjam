using UnityEngine;
using Zenject;

namespace DefaultNamespace.Health
{
    public class EnemyBehaviour : MonoBehaviour
    {
        [SerializeField]
        private int damage = 10;

        [Inject]
        private SignalBus _signalBus;

        [Inject]
        private HealthHolder _healthHolder;
        
        private void Start()
        {
            _signalBus.Subscribe<EnemyActionSignal>(OnEnemyAction);
        }

        private void OnEnemyAction()
        {
            _healthHolder.PlayerHealth.Damage(damage);
        }
    }
}