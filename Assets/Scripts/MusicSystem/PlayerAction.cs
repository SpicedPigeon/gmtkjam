﻿namespace DefaultNamespace
{
    public class PlayerAction : BeatAction
    {
        public PlayerAction(int offset) : base(offset)
        {
        }

        public override Category CAT { get; } = BeatAction.Category.PLAYER_ACTION;

        public override object Signal
        {
            get
            {
                return new PlayerActionSignal();
            }
        }

        public override object PreSignal(BeatAction.Category nextActionCategory)
        {
            return new PrePlayerActionSignal(nextActionCategory);
        }
    }
}
