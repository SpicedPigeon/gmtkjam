﻿using UnityEngine;
using Zenject;

namespace DefaultNamespace
{
    public class TestCube : MonoBehaviour
    {
        public bool preBeat = false;
        private int scale = 2;
        private float timeToGetSmallinMs = 0.499f;
        private float timeOfLastBeat = 0f;

        [Inject]
        private readonly SignalBus _signalBus;

        void Start()
        {
            if (!preBeat)
                _signalBus.Subscribe<BeatSignal>(OnBeat);
            else
                _signalBus.Subscribe<PreBeatSignal>(OnBeat);
        }

        public void OnBeat()
        {
            //if(s.Animation.Equals("test"))
                timeOfLastBeat = UnityEngine.Time.time;
        }

        public void OnBeat2(PreAnimationActionSignal s)
        {
            if (s.Animation.Equals("test"))
                timeOfLastBeat = UnityEngine.Time.time;
        }

        // Update is called once per frame
        void Update()
        {

            float lerpValue = (UnityEngine.Time.time - timeOfLastBeat) / timeToGetSmallinMs;

            transform.localScale = Vector3.Lerp(Vector3.one * scale, Vector3.one, lerpValue);
        }
    }
}
