﻿namespace DefaultNamespace
{
    public class BackgroundAction : BeatAction
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="offset">offset from start of sequence</param>
        /// <param name="ID"> identifier</param>
        public BackgroundAction(int offset) : base(offset)
        {
        }

        public override Category CAT { get; } = BeatAction.Category.BACKGROUND;

        public override object Signal
        {
            get
            {
                return new BackgroundActionSignal();
            }
        }

        public override object PreSignal(BeatAction.Category nextActionCategory)
        {
            return new BackgroundActionSignal();
        }
    }
}
