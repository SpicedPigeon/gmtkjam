﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace DefaultNamespace
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicSystem : MonoBehaviour
    {
        //[Range(-0.1f,0f)]
        //public float beatStartOffset = -0.001f;
        public TrackBuilder Track;
        public float PreBeatSignal = 0.25f;
        private Track activeTrack;
        private AudioSource audiosource;
        private float startTime;

        //Current song position, in seconds
        private float songPosition;

        //Audio that plays sound effects
        public AudioSource AudioSourceSFX;

        [Inject]
        private SignalBus _signalBus;

        private bool playing = false;

        public void Awake()
        {
            activeTrack = Track.Build(); //build my track
        }

        public void Start()
        {
            activeTrack.SetSignalBus(_signalBus);
            audiosource = GetComponent<AudioSource>();
            Play();

            if(!AudioSourceSFX)
            {
                AudioSourceSFX = gameObject.AddComponent<AudioSource>();
                AudioSourceSFX.volume = audiosource.volume - 0.1f;
            }
        }

        public void Play()
        {
            playing = true;
            startTime = (float)AudioSettings.dspTime;
            audiosource.Play();
        }

        bool paused = false;
        float pausedTime = 0f;
        public void TogglePause()
        {
            if(!paused)
            {
                audiosource.Pause();
                Time.timeScale = 0;
                paused = true;

                _signalBus.Fire<PauseSignal>();
            }
            else
            {
                audiosource.UnPause();
                Time.timeScale = 1;
                paused = false;

                _signalBus.Fire<UnpauseSignal>();
            }
        }

        private int lastFrameBeat = -1;
        private bool preBeatSignalSent = false;

        public void Update()
        {
            if(paused)
            {
                pausedTime += Time.unscaledDeltaTime;
            }

            if(Input.GetButtonUp("Pause"))
            {
                TogglePause();
            }
            
            //calculate current song position
            songPosition = (float)(AudioSettings.dspTime - startTime - activeTrack.BeatStartOffset - pausedTime);

            //check sequence tells
            List<Sequence> nextOrCurrentSequences = activeTrack.GetSequencesToTell(songPosition);
            if (nextOrCurrentSequences.Count > 0)
            {
                foreach(Sequence s in nextOrCurrentSequences)
                {
                    s.PlayAudioTell(AudioSourceSFX);
                }
            }

            //check on beat signals
            if (CurrentBeat != lastFrameBeat)
            {
                OnBeat(CurrentBeat);
                lastFrameBeat = CurrentBeat;
            }

            //check pre beat signals
            if (TimeUntilNextBeat() < PreBeatSignal && !preBeatSignalSent)
            {
                OnPreBeat(CurrentBeat + 1);
                preBeatSignalSent = true;
            }
            else if (TimeUntilNextBeat() >= PreBeatSignal)
            {
                preBeatSignalSent = false;
            }

            if(songPosition > audiosource.clip.length)
            {
                PlayerPrefs.SetInt("WON", 0);
                PlayerPrefs.SetString("TIME", FindObjectOfType<TimeVisualizer>()._text.text);
                PlayerPrefs.Save();
                SceneManager.LoadScene(2);
            }
        }

        public Track GetTrack()
        {
            return activeTrack;
        }

        public int BPM
        {
            get
            {
                return activeTrack.BPM;
            }
        }

        public int CurrentBeat
        {
            get
            {
                return activeTrack.CurrentBeat(songPosition);
                //return Mathf.FloorToInt(songPosition * (activeTrack.BPM / 60));
            }
        }

        public float TimeUntilNextBeat()
        {
            return activeTrack.TimeToNextBeat(songPosition);
            //return SecondsPerBeat - songPosition % SecondsPerBeat;
        }

        public float SongPosition
        {
            get
            {
                return songPosition;
            }
        }

        private float BeatsPerSecond
        {
            get
            {
                return activeTrack.BPM / 60f;
            }
        }

        private float SecondsPerBeat
        {
            get
            {
                return 60f / activeTrack.BPM;
            }
        }

        private void OnBeat(int num)
        {
            activeTrack.OnBeat(num);
        }

        private void OnPreBeat(int num)
        {
            activeTrack.OnPreBeat(num, TimeUntilNextBeat());
        }


        void OnDrawGizmos()
        {
            //dont draw gizmo if there is no active track
            if (activeTrack == null)
            {
                return;
            };

            //Draws beat cube with gesture window
            float timeScale = Screen.width * 0.1f; //1 second = 10% of screen width
            float gestureWindow = timeScale * 0.5f;

            Vector3 beatLineLeftTop = new Vector3(Screen.width / 2 - gestureWindow, Screen.height * 0.17f, Camera.main.nearClipPlane + 0.1f);
            Vector3 beatLineRightBottom = new Vector3(Screen.width / 2 + gestureWindow, Screen.height * 0.08f, Camera.main.nearClipPlane + 0.1f);


            Vector3 beatLineLeftBottom = Camera.main.ScreenToWorldPoint(new Vector3(beatLineLeftTop.x, beatLineRightBottom.y, Camera.main.nearClipPlane + 0.1f));
            Vector3 beatLineRightTop = Camera.main.ScreenToWorldPoint(new Vector3(beatLineRightBottom.x, beatLineLeftTop.y, Camera.main.nearClipPlane + 0.1f));
            Vector3 beatLineCenterTop = Camera.main.ScreenToWorldPoint(new Vector3((beatLineLeftTop.x + beatLineRightBottom.x) / 2, beatLineLeftTop.y, Camera.main.nearClipPlane + 0.1f));
            Vector3 beatLineCenterBottom = Camera.main.ScreenToWorldPoint(new Vector3((beatLineLeftTop.x + beatLineRightBottom.x) / 2, beatLineRightBottom.y, Camera.main.nearClipPlane + 0.1f));
            beatLineLeftTop = Camera.main.ScreenToWorldPoint(beatLineLeftTop);
            beatLineRightBottom = Camera.main.ScreenToWorldPoint(beatLineRightBottom);

            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(beatLineLeftTop, beatLineLeftBottom);
            Gizmos.DrawLine(beatLineCenterTop, beatLineCenterBottom);
            Gizmos.DrawLine(beatLineRightTop, beatLineRightBottom);

            //draw "perfect" beat line
            //Gizmos.DrawLine(beatLineCenter + new Vector3(0, scale.y / 2, 0), beatLineCenter - new Vector3(0, scale.y / 2, 0));

            //draw lines for all beats
            foreach (float beat in activeTrack.GetAllBeatTimes())
            {
                float actualBeat = beat + activeTrack.BeatStartOffset;

                //Draws beat
                Vector3 beatStart = new Vector3(Screen.width / 2, Screen.height * 0.15f, Camera.main.nearClipPlane + 0.1f);
                Vector3 beatEnd = new Vector3(Screen.width / 2, Screen.height * 0.1f, Camera.main.nearClipPlane + 0.1f);

                beatStart.x += (actualBeat - songPosition) * timeScale;
                beatEnd.x += (actualBeat - songPosition) * timeScale;

                beatStart = Camera.main.ScreenToWorldPoint(beatStart);
                beatEnd = Camera.main.ScreenToWorldPoint(beatEnd);

                Gizmos.color = Color.green;
                Gizmos.DrawLine(beatStart, beatEnd);
            }
        }
    }
}
