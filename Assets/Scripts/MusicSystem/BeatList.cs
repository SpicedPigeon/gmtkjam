﻿using UnityEngine;

public class BeatList
{
    private float[] _beatTimes;
    private int BPM;

    public BeatList(int length, int repeatTimes, int BPM, int syncopeAt)
    {
        _beatTimes = new float[length * repeatTimes];
        this.BPM = BPM;

        for (int j = 0; j < repeatTimes; j++)
        {
            int offset = j * length;

            for (int i = 0; i < length; i++)
            {
                if (i != syncopeAt)
                {
                    _beatTimes[offset + i] = (60f / BPM) * (offset + i);
                }
                //add syncope
                else
                {
                    _beatTimes[offset + i] = (60f / BPM) * (offset + i) - 0.125f;
                }
            }
        }
    }

    public float[] BeatTimes
    {
        get
        {
            return _beatTimes;
        }
    }

    public int CurrentBeat(float songTime)
    {
        if (songTime < 0.5f)
            return 0;

        int startIndex = Mathf.Max(0, Mathf.FloorToInt(songTime / (60f / BPM)) - 1);

        for(int i = 0; i < _beatTimes.Length; i++)
        {
            if(songTime <= _beatTimes[i])
            {
                return i - 1;
            }
        }

        return _beatTimes.Length - 1;
    }

    public float TimeToNextBeat(float songTime)
    {
        int currentBeat = CurrentBeat(songTime);

        //last beat
        if (currentBeat == _beatTimes.Length - 1)
            return 0f;

        float nextBeatTime = _beatTimes[currentBeat + 1];
        
        return nextBeatTime - songTime;
    }
}
