﻿using System.Collections.Generic;
using Zenject;

//Hold all information when which sequence appears
namespace DefaultNamespace
{
    public class Track
    {
        private float _beatStartOffset;
        private SignalBus _signalBus;
        private int _BPM;
        private List<Sequence> _sequences;
        private int lengthInBeats;
        private BeatList customBeatList = new BeatList(8, 65, 120, 6);

        public Track(float beatStartOffset, int BPM)
        {
            this._beatStartOffset = beatStartOffset;
            this._BPM = BPM;
            Sequences = new List<Sequence>();
        }

        public void AddSequence(Sequence s)
        {
            Sequences.Add(s);
        }

        public void AddSequences(List<Sequence> sList)
        {
            Sequences.AddRange(sList);
        }

        public void SetSignalBus(SignalBus signalBus) 
        {
            _signalBus = signalBus;
        }

        public float BeatStartOffset
        {
            get
            {
                return _beatStartOffset;
            }
        }

        public int BPM
        {
            get
            {
                return _BPM;
            }
        }

        public BeatAction[] GetBeatActions()
        {
            int amount = customBeatList.BeatTimes.Length;
            BeatAction[] beatActions = new BeatAction[amount];
            
                foreach (Sequence s in _sequences)
                {
                    foreach (BeatAction a in s.GetActions())
                    {
                        if (a.CAT != BeatAction.Category.ENEMY_ACTION && a.CAT != BeatAction.Category.PLAYER_ACTION)
                        {
                            continue;
                        }
                        beatActions[s.Offset + a.Offset] = a;
                    }
                }
            return beatActions;
        }

        public List<Sequence> Sequences { get => _sequences; private set => _sequences = value; }

        //before this beat happens
        public void OnPreBeat(int num, float timeToBeat)
        {
            //fire generic signal
            _signalBus.Fire(new PreBeatSignal(timeToBeat));

            //fire action signal
            foreach (Sequence s in _sequences)
            {
                //are we at the start of a sequence?
                if(num == s.Offset)
                {
                    _signalBus.Fire(new SequenceStartSignal());
                }
                //are we at the end of the sequence?
                else if (num == s.Offset + s.Length)
                {
                    _signalBus.Fire(new SequenceEndSignal());
                }


                foreach (BeatAction a in s.GetActions())
                {
                    //is there an action on this beat
                    if (num == s.Offset + a.Offset)
                    {
                        BeatAction nextBeatAction = GetFollowingAction(num, 1);
                        BeatAction.Category cat = nextBeatAction != null ? nextBeatAction.CAT : BeatAction.Category.BACKGROUND;


                        object signal = a.PreSignal(cat);
                        _signalBus.Fire(signal);
                        
                    }

                    //one beat before enemy beat
                    if (num + 1 == s.Offset + a.Offset)
                    {
                        if (a.CAT == BeatAction.Category.ENEMY_ACTION)
                        {
                            _signalBus.Fire(new OnePreBeatBeforeEnemyActionSignal());
                        }
                    }
                }
            }
        }

        //when this beat happens
        public void OnBeat(int num)
        {
            _signalBus.Fire<BeatSignal>();
            
            foreach (Sequence s in Sequences)
            {
                foreach (BeatAction a in s.GetActions())
                {
                    if (num == s.Offset + a.Offset)
                    {
                        BeatAction nextBeatAction = GetFollowingAction(num, 1);
                        BeatAction.Category cat = nextBeatAction != null ? nextBeatAction.CAT : BeatAction.Category.BACKGROUND;

                        _signalBus.Fire(new ActionSignal(cat));
                        _signalBus.Fire(a.Signal);
                        _signalBus.Fire<PostActionSignal>();
                    }
                }
            }
            _signalBus.Fire<PostBeatSignal>();
        }

        public List<Sequence> GetSequencesToTell(float songTime)
        {
            List<Sequence> nextOrCurrentSequences = new List<Sequence>();
            foreach(Sequence s in _sequences)
            {
                if(s.ShouldPlayTell(songTime, BPM))
                {
                    nextOrCurrentSequences.Add(s);
                }
            }
            return nextOrCurrentSequences;
        }

        private BeatAction GetFollowingAction(int offset, int skips)
        {
            int length = GetLengthInBeats();
            int currentSkips = 0;
            offset += 1;

            while (offset < length)
            {
                foreach (Sequence s in _sequences)
                {
                    foreach (BeatAction a in s.GetActions())
                    {
                        //ignore animation actions
                        if (offset == s.Offset + a.Offset && a.CAT != BeatAction.Category.ANIMATION_ACTION)
                        {
                            if(currentSkips < skips)
                            {
                                currentSkips++;
                            }
                            else
                            {
                                return a;
                            }
                        }
                    }
                }
                offset += 1;
            }

            return null;
        }

        private int GetLengthInBeats()
        {
            int result = 0;

            foreach (Sequence s in _sequences)
            {
                if(result < s.Offset + s.Length)
                {
                    result = s.Offset + s.Length;
                }
            }

            return result;
        }

        public int CurrentBeat(float songTime)
        {
            return customBeatList.CurrentBeat(songTime);
        }

        public float TimeToNextBeat(float songTime)
        {
            return customBeatList.TimeToNextBeat(songTime);
        }

        public float[] GetAllBeatTimes()
        {
            /* List<float> allTimes = new List<float>();

             for(int i = 0; i < 360; i++)
             {
                 allTimes.Add(i * 0.5f);
             }

             return allTimes.ToArray();*/
            return customBeatList.BeatTimes;
        }
    }

    public class PreBeatSignal
    {
        public PreBeatSignal(float timeToBeat)
        {
            this._timeToBeat = timeToBeat;
        }

        private float _timeToBeat;
        public float TimeToBeat
        {
            get
            {
                return _timeToBeat;
            }
        }

    }
    public class BeatSignal
    {
    }
    public class PostBeatSignal
    {
    }
}