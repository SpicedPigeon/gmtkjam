﻿using System.Collections.Generic;
using UnityEngine;

namespace DefaultNamespace
{
    [CreateAssetMenu(fileName = "Track", menuName = "ScriptableObjects/Track", order = 1)]
    public class TrackBuilder : ScriptableObject
    {
        public float BeatStartOffset = 0.003f;
        public int BPM = 120;
        [SerializeField]
        public List<SequenceListBlueprint> foregroundSequences = new List<SequenceListBlueprint>();
        [SerializeField]
        public List<BackgroundSequenceBlueprint> backgroundSequences = new List<BackgroundSequenceBlueprint>();

        public Track Build()
        {
            Track t = new Track(BeatStartOffset, BPM);
            
            //build foreground sequences
            foreach(SequenceListBlueprint blueprint in foregroundSequences)
            {
                t.AddSequences(blueprint.Build());
            }

            //build background sequences
            foreach(BackgroundSequenceBlueprint bsb in backgroundSequences)
            {
                t.AddSequence(bsb.Build());
            }

            return t;
        }

        [System.Serializable]
        public class SequenceListBlueprint
        {
            public int[] OffsetsInSong;
            public SequenceBlueprint sequenceBlueprint;

            public List<Sequence> Build()
            {
                List<Sequence> sequences = new List<Sequence>();

                //for each offset in song create the specified Sequence
                foreach (int o in OffsetsInSong)
                {
                    //build the sequence multiple times
                    sequences.Add(sequenceBlueprint.Build(o));
                }

                return sequences;
            }
        }

        [System.Serializable]
        public class SequenceBlueprint
        {
            public int Length;
            public AudioClip tell;
            public float preBeatTime;
            public List<BeatBlueprint> beatActions = new List<BeatBlueprint>();

            public virtual Sequence Build(int offset)
            {
                Sequence s = new Sequence(offset, Length, tell, preBeatTime);

                //add each beat action
                foreach (BeatBlueprint bb in beatActions)
                {
                    //build each action
                    s.AddBeatAction(bb.Build());
                }

                return s;
            }
        }

        [System.Serializable]
        public class BackgroundSequenceBlueprint
        {
            public int Start;
            public int End;
            public int Delay;
            public BeatBlueprint beatAction;

            public Sequence Build()
            {
                Sequence s = new Sequence(Start, End - Start, null, 0);
                
                for(int i = Start; i <= End;  i += Delay)
                {
                    s.AddBeatAction(new BackgroundAction(i - Start));
                }

                return s;
            }
        }

        [System.Serializable]
        public class BeatBlueprint
        {
            public int offset;
            public string animation;
            public BeatAction.Category type;

            public BeatAction Build()
            {
                switch(type)
                {
                    case BeatAction.Category.BACKGROUND:
                        return new BackgroundAction(offset);

                    case BeatAction.Category.ENEMY_ACTION:
                        return new EnemyAction(offset);

                    case BeatAction.Category.PLAYER_ACTION:
                        return new PlayerAction(offset);

                    case BeatAction.Category.ANIMATION_ACTION:
                        if (animation.Length == 0 || animation.Equals(""))
                        {
                            Debug.LogWarning("Added Beat Animation Action with empty string! Is this intended?");
                        }
                        return new AnimationAction(offset, animation);

                    default:
                        Debug.LogWarning("Trackbuilder Warning: Could not build beat action because it was null.");
                        return null;
                }
            }
        }
    }
}
