﻿namespace DefaultNamespace
{
    public abstract class BeatAction
    {
        private readonly int _offset = 0;

        public BeatAction(int offset)
        {
            this._offset = offset;
        }

        public int Offset
        {
            get
            {
                return _offset;
            }
        }


        public enum Category
        {
            BACKGROUND, PLAYER_ACTION, ENEMY_ACTION, ANIMATION_ACTION
        }

        public abstract Category CAT { get; }

        public virtual object Signal
        {
            get
            {
                return null;
            }
        }

        public virtual object PreSignal(BeatAction.Category nextActionCategory)
        {
            return null;
        }
    }
}
