﻿namespace DefaultNamespace
{
    public class AnimationAction : BeatAction
    {
        private string _animation;

        public AnimationAction(int offset, string animation) : base(offset)
        {
            this._animation = animation;
        }

        public override Category CAT { get; } = BeatAction.Category.ANIMATION_ACTION;

        public override object Signal
        {
            get
            {
                return new AnimationActionSignal(_animation);
            }
        }

        public override object PreSignal(BeatAction.Category nextActionCategory)
        {
            return new PreAnimationActionSignal(_animation);
        }
    }
}
