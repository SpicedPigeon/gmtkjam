﻿using System.Collections.Generic;
using UnityEngine;

//Holds all information when which beat action occurs
namespace DefaultNamespace
{
    public class Sequence
    {
        private int _offset;
        private int _length;
        private AudioClip _audioClip;
        private float _preBeatTime;
        private bool hasPlayedClip = false;

        public Sequence(int offset, int length, AudioClip audioClip, float preBeatTime)
        {
            this._offset = offset;
            this._length = length;
            this._audioClip = audioClip;
            this._preBeatTime = preBeatTime;
            this._beatActions = new List<BeatAction>();
        }
        
        public int Offset
        {
            get
            {
                return _offset;
            }
        }

        public int Length
        {
            get
            {
                return _length;
            }
        }

        public void PlayAudioTell(AudioSource s)
        {
            if (s != null && _audioClip != null)
            {
                s.clip = _audioClip;
                s.Play();
            }
            hasPlayedClip = true;
        }

        public float PreBeatTime
        {
            get
            {
                return _preBeatTime;
            }
        }

        private List<BeatAction> _beatActions;

        public void AddBeatAction(BeatAction beatAction)
        {
            _beatActions.Add(beatAction);
        }

        public BeatAction[] GetActions()
        {
            return _beatActions.ToArray();
        }

        public bool ShouldPlayTell(float songTime, int BPM)
        {
            float beatsPerSecond = 60f / BPM;
            float offsetInSeconds = (_offset - 1) * beatsPerSecond;

            return songTime >= offsetInSeconds - _preBeatTime && !hasPlayedClip;
        }
    }
}
