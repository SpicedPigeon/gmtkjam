﻿namespace DefaultNamespace
{
    public class EnemyAction : BeatAction
    {
        public EnemyAction(int offset) : base(offset)
        {
        }

        public override Category CAT { get; } = BeatAction.Category.ENEMY_ACTION;

        public override object Signal
        {
            get
            {
                return new EnemyActionSignal();
            }
        }

        public override object PreSignal(BeatAction.Category nextActionCategory)
        {
            return new PreEnemyActionSignal(nextActionCategory);
        }
    }
}
