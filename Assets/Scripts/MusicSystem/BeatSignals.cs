﻿namespace DefaultNamespace
{

    public class ActionSignal
    {
        private BeatAction.Category c;

        public ActionSignal(BeatAction.Category nextActionCategory)
        {
            c = nextActionCategory;
        }

        public BeatAction.Category NextActionCategory
        {
            get
            {
                return c;
            }
        }
    }

    public class PostActionSignal
    {
    }

    public class PrePlayerActionSignal
    {
        private BeatAction.Category c;
        public PrePlayerActionSignal(BeatAction.Category nextActionCategory)
        {
            c = nextActionCategory;
        }

        public BeatAction.Category NextActionCategory
        {
            get
            {
                return c;
            }
        }
    }

    public class PreEnemyActionSignal
    {
        private BeatAction.Category c;

        public PreEnemyActionSignal(BeatAction.Category nextActionCategory)
        {
            c = nextActionCategory;
        }

        public BeatAction.Category NextActionCategory
        {
            get
            {
                return c;
            }
        }
    }

    public class PreAnimationActionSignal
    {
        private string _animation;

        public PreAnimationActionSignal(string animation)
        {
            this._animation = animation;
        }

        public string Animation
        {
            get
            {
                return _animation;
            }
        }
    }


    public class PlayerActionSignal
    {
    }

    public class OnePreBeatBeforeEnemyActionSignal
    {

    }

    public class EnemyActionSignal
    {
    }

    public class AnimationActionSignal
    {
        private string _animation;

        public AnimationActionSignal(string animation)
        {
            this._animation = animation;
        }

        public string Animation
        {
            get
            {
                return _animation;
            }
        }
    }

    public class BackgroundActionSignal
    {
    }
    
    public class SequenceStartSignal
    {
    }

    public class SequenceEndSignal
    {
    }
}
