using DefaultNamespace.Health;
using UnityEngine;
using Zenject;

namespace DefaultNamespace.VisualEffects
{
    public class AnimationPlayerOnPlayerAction : MonoBehaviour
    {
        [SerializeField]
        private Animation _animation;

        [Inject]
        private SignalBus _signalBus;
        
        private void Start()
        {
            _signalBus.Subscribe<AfterDamageReceivedSignal>(OnAction);
        }

        private void OnAction()
        {
            _animation.Play();
        }
    }
}