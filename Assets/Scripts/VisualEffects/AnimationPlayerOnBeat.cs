using UnityEngine;
using Zenject;

namespace DefaultNamespace.VisualEffects
{
    public class AnimationPlayerOnBeat : MonoBehaviour
    {
        [SerializeField]
        private Animation _animation;

        [Inject]
        private SignalBus _signalBus;
        
        private void Start()
        {
            _signalBus.Subscribe<PreBeatSignal>(OnAction);
        }

        private void OnAction()
        {
            _animation.Play();
        }
    }
}