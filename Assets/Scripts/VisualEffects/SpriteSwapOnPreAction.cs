using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DefaultNamespace.VisualEffects
{
    public class SpriteSwapOnPreAction : MonoBehaviour
    {
        [SerializeField]
        private Image _targetImage;

        [SerializeField]
        private Sprite _playerSprite;

        [SerializeField]
        private Sprite _enemySprite;

        [Inject]
        private SignalBus _signalBus;

        private void Start()
        {
            _signalBus.Subscribe<PreAnimationActionSignal>(OnAnimationBeat);
            _signalBus.Subscribe<EnemyActionSignal>(OnAction);
            OnAction();
        }

        void OnAnimationBeat(PreAnimationActionSignal s)
        {
            if (s.Animation == "windup")
            {
                _targetImage.sprite = _enemySprite;
            }
        }
        
        private void OnAction()
        {
            _targetImage.sprite = _playerSprite;   
        }
    }
}