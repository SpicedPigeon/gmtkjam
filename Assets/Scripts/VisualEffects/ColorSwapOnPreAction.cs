using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace DefaultNamespace.VisualEffects
{
    public class ColorSwapOnPreAction : MonoBehaviour
    {
        [SerializeField]
        private Image _targetImage;

        [SerializeField]
        private Color _enemyColor;

        [SerializeField]
        private Color _playerColor;

        [Inject]
        private SignalBus _signalBus;

        private void Start()
        {
            _signalBus.Subscribe<PreAnimationActionSignal>(OnAnimationBeat);
            _signalBus.Subscribe<EnemyActionSignal>(OnAction);
            OnAction();
        }

        void OnAnimationBeat(PreAnimationActionSignal s)
        {
            if (s.Animation == "windup")
            {
                _targetImage.color = _enemyColor;
            }
        }
        
        private void OnAction()
        {
                _targetImage.color = _playerColor;
        }
    }
}