using UnityEngine;
using Zenject;

namespace DefaultNamespace.Indicator
{
    public class BeatIndicator : MonoBehaviour
    {
        [SerializeField]
        private GameObject _playerIndicatorPrefab;

        [SerializeField]
        private GameObject _enemyIndicatorPrefab;

        [SerializeField]
        private GameObject _beatIndicatorPrefab;

        [SerializeField]
        private Transform _indicatorRoot;

        [Inject]
        private SignalBus _signalBus;

        [Inject]
        private MusicSystem _musicSystem;

        private void Start()
        {
            Track t = _musicSystem.GetTrack();

            foreach (BeatAction beatAction in t.GetBeatActions())
            {
                if (beatAction == null)
                {
                    Instantiate(_beatIndicatorPrefab, _indicatorRoot);
                }
                else if (beatAction.CAT == BeatAction.Category.PLAYER_ACTION)
                {
                    Instantiate(_playerIndicatorPrefab, _indicatorRoot);
                }
                else if(beatAction.CAT == BeatAction.Category.ENEMY_ACTION)
                {
                    Instantiate(_enemyIndicatorPrefab, _indicatorRoot);
                }
                else
                {
                    Instantiate(_beatIndicatorPrefab, _indicatorRoot);
                }
            }

            _signalBus.Subscribe<BeatSignal>(OnBeat);
        }

        private int _counter = -1;

        private void OnBeat()
        {
            _counter++;

            _indicatorRoot.localPosition = new Vector3(_counter * -110 + 185f, 0, 0);
        }
    }
}