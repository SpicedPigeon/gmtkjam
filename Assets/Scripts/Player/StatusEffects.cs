using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DefaultNamespace.Player
{
    public class StatusEffects
    {
        public List<Effect> CurrentEffects = new List<Effect>();

        public void RemoveEffect(Effect effect)
        {
            CurrentEffects.Remove(effect);
        }

        public int GetDamageModified(int cardDamage)
        {
            List<Effect> damageModifier = CurrentEffects.Where(effect => effect.GetType() == typeof(DodgeDamageModifier)).ToList();
            
            if (damageModifier.Count != 0)
            {
                foreach (Effect effect in damageModifier)
                {
                    cardDamage += ((DodgeDamageModifier) effect).Amount;
                    Debug.Log("Added dodge attack");
                    CurrentEffects.Remove(effect);
                }
            }
            
            return cardDamage;
        }

        public int GetDamageModifiedPreview(int cardDamage)
        {
            List<Effect> damageModifier = CurrentEffects.Where(effect => effect.GetType() == typeof(DodgeDamageModifier)).ToList();
            
            if (damageModifier.Count != 0)
            {
                foreach (Effect effect in damageModifier)
                {
                    cardDamage += ((DodgeDamageModifier) effect).Amount;
                }
            }
            
            return cardDamage;
        }
    }
}