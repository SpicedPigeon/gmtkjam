using Zenject;

namespace DefaultNamespace.Player
{
    public abstract class Effect
    {
        protected readonly SignalBus _signalBus;

        protected readonly StatusEffects _statusEffects;
        
        protected Effect(SignalBus signalBus, StatusEffects statusEffects)
        {
            _signalBus = signalBus;
            _statusEffects = statusEffects;
        }
    }
}