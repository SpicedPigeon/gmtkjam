using DefaultNamespace.Health;
using UnityEngine;
using Zenject;

namespace DefaultNamespace.Player
{
    public class DodgeEffect : Effect
    {
        private bool _ignoreFirst;
        
        public DodgeEffect(SignalBus signalBus, StatusEffects statusEffects) : base(signalBus, statusEffects)
        {
            signalBus.Subscribe<PostActionSignal>(OnBeatEnded);
            signalBus.Subscribe<BeforeDamageReceivedSignal>(OnGetDamage);
            
            _statusEffects.CurrentEffects.Add(new DodgeDamageModifier(_signalBus, _statusEffects)
            {
                Amount = 5
            });
        }

        private void OnBeatEnded()
        {
            if (!_ignoreFirst)
            {
                _ignoreFirst = true;
                return;
            }
            
            _statusEffects.RemoveEffect(this);
            _signalBus.Unsubscribe<PostActionSignal>(OnBeatEnded);
            _signalBus.Unsubscribe<BeforeDamageReceivedSignal>(OnGetDamage);
        }

        private void OnGetDamage(BeforeDamageReceivedSignal signal)
        {
            Debug.Log("Dodged");
            signal.PlayerHealth.AmountOfDamageReduction = signal.Amount;
        }
    }
}