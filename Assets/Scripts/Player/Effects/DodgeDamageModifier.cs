using DefaultNamespace.Health;
using UnityEngine;
using Zenject;

namespace DefaultNamespace.Player
{
    public class DodgeDamageModifier : Effect
    {
        public int Amount;
        
        public DodgeDamageModifier(SignalBus signalBus, StatusEffects statusEffects) : base(signalBus, statusEffects)
        {
            signalBus.Subscribe<AfterDamageReceivedSignal>(OnDamageReceived);
        }

        private void OnDamageReceived()
        {
            Debug.Log("Lost dodge attack boost");
            _statusEffects.RemoveEffect(this);
            _signalBus.Unsubscribe<AfterDamageReceivedSignal>(OnDamageReceived);
        }
    }
}