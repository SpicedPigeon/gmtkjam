﻿using System.Collections;
using Zenject;
using UnityEngine;

namespace DefaultNamespace
{
    public class BeatSFX : MonoBehaviour
    {
        [Inject]
        private readonly SignalBus _signalBus;

        [SerializeField]
        private AudioSource audioSource;

        [SerializeField]
        private AudioClip enemyBeatSFX;
        [SerializeField]
        private AudioClip anticipationBeatSFX;

        [SerializeField]
        private float offsetEnemyBeat;

        [SerializeField]
        private float offsetAnticipationBeat;

        void Start()
        {
            _signalBus.Subscribe<PreEnemyActionSignal>(OnPreEnemyBeat);
            _signalBus.Subscribe<PreAnimationActionSignal>(OnPreAnticipationBeat);
        }

        void OnPreEnemyBeat()
        {
            StartCoroutine(PlayDelayed(enemyBeatSFX, offsetEnemyBeat));
        }

        void OnPreAnticipationBeat(PreAnimationActionSignal s)
        {
            if (s.Animation.Equals("windup"))
            { StartCoroutine(PlayDelayed(anticipationBeatSFX, offsetAnticipationBeat)); }
        }

        private IEnumerator PlayDelayed(AudioClip clip, float delay)
        {
            yield return new WaitForSeconds(delay);
            audioSource.PlayOneShot(clip);
        }
    }
}
