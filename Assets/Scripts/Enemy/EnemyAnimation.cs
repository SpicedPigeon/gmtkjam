﻿using System.Collections;
using Zenject;
using UnityEngine;

namespace DefaultNamespace
{
    public class EnemyAnimation : MonoBehaviour
    {
        [Inject]
        private readonly SignalBus _signalBus;

        [SerializeField]
        private Animator animator;

        [SerializeField]
        private GameObject _pow;
        
        
        [SerializeField]
        private Animation _animatorHealthBar;
        [SerializeField]
        private GameObject _damageNumber;
        
        void Start()
        {
            _signalBus.Subscribe<PreEnemyActionSignal>(OnPreEnemyAction);// attack

            _signalBus.Subscribe<PreBeatSignal>(OnPreBeat);// bounce
            _signalBus.Subscribe<PreAnimationActionSignal>(OnAnimationBeat); // anticipation
        }

        void OnPreEnemyAction()
        {
            animator.SetTrigger("attack");
        }

        void OnPreBeat()
        {
            animator.SetTrigger("bounce");
        }

        void OnAnimationBeat(PreAnimationActionSignal s)
        {
            animator.SetTrigger(s.Animation);
        }

        public void EnablePow()
        {
            _pow.SetActive(true);
            _animatorHealthBar.Play();
            _damageNumber.SetActive(true);

            StartCoroutine(DisablePow());

        }

        public IEnumerator DisablePow()
        {
            yield return new WaitForSeconds(0.3f);
            _pow.SetActive(false);
            _damageNumber.SetActive(false);
        }
    }
}