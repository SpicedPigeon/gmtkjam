﻿using UnityEngine;
using Zenject;

namespace DefaultNamespace
{
    public class DebugUI : MonoBehaviour
    {
        [Inject]
        private MusicSystem _musicSystem;

        private void OnGUI()
        {
            Rect rect = new Rect(Screen.width - 100, Screen.height - 100, 100, 100);
            GUI.Label(rect, "Time: " + _musicSystem.SongPosition
                + "\nBPM: " + _musicSystem.BPM
                + "\nBeat: " + _musicSystem.CurrentBeat
                + "\nTimeUntilNext: " + _musicSystem.TimeUntilNextBeat());
        }
    }
}
