using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class FinishInitializer : MonoBehaviour
    {
        [SerializeField]
        private Sprite _first;
        
        [SerializeField]
        private Sprite _second;

        [SerializeField]
        private TMP_Text _finishText;

        [SerializeField]
        private Image _image;

        private void Start()
        {
            if (PlayerPrefs.GetInt("WON") == 1)
            {
                _image.sprite = _first;
                _finishText.text = "You win!\n";
            }
            else
            {
                _image.sprite = _second;
                _finishText.text = "You almost won!\n";
            }

            string time = PlayerPrefs.GetString("TIME");

            _finishText.text += time;
        }

        public void ReturnToMainMenu()
        {
            SceneManager.LoadScene(0);
        }
    }
}