using Cards;
using DefaultNamespace.Health;
using DefaultNamespace.Player;
using UnityEngine;
using Zenject;

namespace DefaultNamespace
{
    public class GameInstaller : MonoInstaller
    {
        [SerializeField]
        private MusicSystem _musicSystem;

        [SerializeField]
        private HealthHolder _healthHolder;

        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);

            Container.DeclareSignal<ActionSignal>();
            Container.DeclareSignal<PostActionSignal>();

            Container.DeclareSignal<SequenceStartSignal>();
            Container.DeclareSignal<SequenceEndSignal>();

            Container.DeclareSignal<PrePlayerActionSignal>();
            Container.DeclareSignal<PreEnemyActionSignal>();
            Container.DeclareSignal<PreAnimationActionSignal>();
            Container.DeclareSignal<OnePreBeatBeforeEnemyActionSignal>();

            Container.DeclareSignal<PlayerActionSignal>();
            Container.DeclareSignal<EnemyActionSignal>();
            Container.DeclareSignal<BackgroundActionSignal>();
            Container.DeclareSignal<AnimationActionSignal>();

            Container.DeclareSignal<BeforeDamageReceivedSignal>();
            Container.DeclareSignal<AfterDamageReceivedSignal>();

            Container.DeclareSignal<PreBeatSignal>();
            Container.DeclareSignal<BeatSignal>();
            Container.DeclareSignal<PostBeatSignal>();

            Container.DeclareSignal<PauseSignal>();
            Container.DeclareSignal<UnpauseSignal>();

            Container.BindInterfacesAndSelfTo<StatusEffects>().AsSingle();
            Container.BindInterfacesAndSelfTo<CounterCardCountingInstance>().AsSingle();
            Container.BindInterfacesAndSelfTo<MusicSystem>().FromInstance(_musicSystem).AsSingle();
            Container.BindInterfacesAndSelfTo<HealthHolder>().FromInstance(_healthHolder).AsSingle();
        }
    }

    public class PauseSignal
    {
    }

    public class UnpauseSignal
    {

    }
}