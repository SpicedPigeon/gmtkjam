using System.Linq;
using Cards;
using DefaultNamespace;
using UnityEngine;
using Zenject;

namespace Interaction
{
    public class CurrentSelectedInteraction : MonoBehaviour
    {
        public HoveringInteraction Current;

        [Inject]
        private SignalBus _signalBus;
        
        private void Start()
        {
            _signalBus.Subscribe<PlayerActionSignal>(OnPlayerAction);
        }

        private void OnPlayerAction()
        {
            if (Current == null || Current.Use == null)
            {
                CardSystem cardSystem = FindObjectOfType<CardSystem>();
                if (!cardSystem.TryDrawCard())
                {
                    cardSystem._handPile.First().PlayCard();
                }
            }
            else
            {
                Current.Use.Invoke();
            }
        }
    }
}