using System.Linq;
using UnityEngine;

namespace Interaction
{
    public class HandSlots : MonoBehaviour
    {
        [SerializeField]
        private Transform[] _roots;

        private bool[] _slotFree;

        private void Awake()
        {
            _slotFree = new bool[_roots.Length];

            for (int i = 0; i < _slotFree.Length; i++)
            {
                _slotFree[i] = true;
            }
        }

        private bool HasFreeSlots()
        {
            return _slotFree.Any(b => b);
        }

        public bool TryGetFreeSlot(out Transform slot)
        {
            slot = null;

            for (int i = 0; i < _slotFree.Length; i++)
            {
                if (_slotFree[i])
                {
                    slot = _roots[i];
                    _slotFree[i] = false;
                    return true;
                }
            }
            
            return false;
        }

        public void FreeSlot(Transform slot)
        {
            for (int i = 0; i < _roots.Length; i++)
            {
                if (_roots[i] == slot)
                {
                    _slotFree[i] = true;
                }
            }
        }
    }
}