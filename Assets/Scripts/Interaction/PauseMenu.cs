﻿using UnityEngine;
using Zenject;

namespace DefaultNamespace
{
    public class PauseMenu : MonoBehaviour
    {
        [Inject]
        private void InjectMethod(SignalBus signalBus)
        {
            signalBus.Subscribe<PauseSignal>(OnPause);
            signalBus.Subscribe<UnpauseSignal>(OnUnPause);
        }

        void OnPause()
        {
            gameObject.SetActive(true);
        }

        void OnUnPause()
        {
            gameObject.SetActive(false);
        }
    }
}
