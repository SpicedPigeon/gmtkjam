using Cards;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Interaction
{
    public class HoveringInteraction : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private CurrentSelectedInteraction _currentSelectedInteraction;
        
        private CurrentSelectedInteractionPauseMenu _currentSelectedInteractionPauseMenu;

        public UnityEvent Use;

        [SerializeField]
        private UnityEvent _onEnableHighlight;
        
        [SerializeField]
        private UnityEvent _onDisableHighlight;

        [SerializeField]
        private bool _isPauseHovering;
        
        private void Awake()
        {
            _currentSelectedInteraction = FindObjectOfType<CurrentSelectedInteraction>();
            _currentSelectedInteractionPauseMenu = FindObjectOfType<CurrentSelectedInteractionPauseMenu>();
        }

        private void EnableHighlight()
        {
            _onEnableHighlight.Invoke();

            if (_isPauseHovering)
            {
                _currentSelectedInteractionPauseMenu.DescriptionText.SetText(GetComponent<Card>().Description);
            }
        }
        
        private void DisableHighlight()
        {
            _onDisableHighlight.Invoke();
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!_isPauseHovering)
            {
                if (_currentSelectedInteraction.Current != null)
                {
                    _currentSelectedInteraction.Current.DisableHighlight();
                }

                _currentSelectedInteraction.Current = this;
            }
            else
            {
                if (_currentSelectedInteractionPauseMenu.Current != null)
                {
                    _currentSelectedInteractionPauseMenu.Current.DisableHighlight();
                }

                _currentSelectedInteractionPauseMenu.Current = this;
            }

            EnableHighlight();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            DisableHighlight();

            if (!_isPauseHovering)
            {
                if (_currentSelectedInteraction.Current == this)
                {
                    _currentSelectedInteraction.Current = null;
                }
            }
            else
            {
                if (_currentSelectedInteractionPauseMenu.Current == this)
                {
                    _currentSelectedInteractionPauseMenu.Current = null;
                }
            }
        }
    }
}