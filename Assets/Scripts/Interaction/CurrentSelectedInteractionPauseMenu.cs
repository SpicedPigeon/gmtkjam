using TMPro;
using UnityEngine;

namespace Interaction
{
    public class CurrentSelectedInteractionPauseMenu : MonoBehaviour
    {
        public HoveringInteraction Current;

        public TMP_Text DescriptionText;
    }
}