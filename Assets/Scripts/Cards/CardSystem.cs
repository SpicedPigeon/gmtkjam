﻿using System;
using System.Collections.Generic;
using Interaction;
using UnityEngine;
using Zenject;
using Random = UnityEngine.Random;

namespace Cards
{
    public class CardSystem : MonoBehaviour
    {
        [SerializeField]
        private List<CardAmount> _startCards;

        [Serializable]
        private struct CardAmount
        {
            public Card Prefab;
            public int Amount;
        }


        /// <summary>
        /// Only prefabs
        /// </summary>
        private List<Card> _drawPile;

        /// <summary>
        /// Instances
        /// </summary>
        [NonSerialized]
        public List<Card> _handPile;

        /// <summary>
        /// Prefabs
        /// </summary>
        private List<Card> _discardPile;

        [SerializeField]
        private int _numberOfStartCards;

        [SerializeField]
        private Transform _handRoot;

        [SerializeField]
        private HandSlots _handSlots;

        [Inject]
        private DiContainer _container;

        [SerializeField]
        private Transform _drawPileRoot;

        [Inject]
        private SignalBus _signalBus;

        private void Start()
        {
            _drawPile = new List<Card>();
            _handPile = new List<Card>();
            _discardPile = new List<Card>();
            foreach (CardAmount cardAmount in _startCards)
            {
                for (int i = 0; i < cardAmount.Amount; i++)
                {
                    _drawPile.Add(cardAmount.Prefab);
                }
            }

            Shuffle();

            for (int i = 0; i < _numberOfStartCards; i++)
            {
                DrawCard();
            }
        }

        public void DrawCard()
        {
            if (_drawPile.Count == 0)
            {
                Shuffle();
            }

            if (_drawPile.Count == 0)
            {
                return;
            }

            if (_handSlots.TryGetFreeSlot(out Transform slot))
            {
                Card cardPrefab = _drawPile[Random.Range(0, _drawPile.Count)];
                _drawPile.Remove(cardPrefab);
                Card cardInstance = _container.InstantiatePrefabForComponent<Card>(cardPrefab, _handRoot);
                cardInstance.ParentPrefab = cardPrefab;
                cardInstance.transform.SetParent(slot);
                cardInstance.transform.position = _drawPileRoot.position;
                cardInstance.PlayDrawCardAnimation();
                _handPile.Add(cardInstance);
            }
            else
            {
                return;
            }

            UpdateCardAmounts();
        }

        public bool TryDrawCard()
        {
            if (_drawPile.Count == 0)
            {
                Shuffle();
            }

            if (_drawPile.Count == 0)
            {
                return false;
            }

            if (_handSlots.TryGetFreeSlot(out Transform slot))
            {
                Card cardPrefab = _drawPile[Random.Range(0, _drawPile.Count)];
                _drawPile.Remove(cardPrefab);
                Card cardInstance = _container.InstantiatePrefabForComponent<Card>(cardPrefab, _handRoot);
                cardInstance.ParentPrefab = cardPrefab;
                cardInstance.transform.SetParent(slot);
                cardInstance.transform.position = _drawPileRoot.position;
                cardInstance.PlayDrawCardAnimation();
                _handPile.Add(cardInstance);
            }
            else
            {
                return false;
            }

            UpdateCardAmounts();
            return true;
        }


        private void Shuffle()
        {
            _drawPile.AddRange(_discardPile);
            _discardPile.Clear();

            for (int i = 0; i < _drawPile.Count; i++)
            {
                Card temp = _drawPile[i];
                int randomIndex = Random.Range(i, _drawPile.Count);
                _drawPile[i] = _drawPile[randomIndex];
                _drawPile[randomIndex] = temp;
            }

            UpdateCardAmounts();
        }

        public void PlayCard(Card cardInstance)
        {
            Card prefab = cardInstance.ParentPrefab;
            _handPile.Remove(cardInstance);
            _discardPile.Add(cardInstance.ParentPrefab);
            //Destroy(cardInstance);
            //StartCoroutine(DiscardCardDelayed(cardInstance, cardInstance.transform.position));
            _handSlots.FreeSlot(cardInstance.transform.parent);
            UpdateCardAmounts();
        }

        public void UpdateCardAmounts()
        {
            //_currentDeckAmount.text = _drawPile.Count.ToString();
            //_discardableDeckAmount.text = _discardPile.Count.ToString();
        }
    }
}