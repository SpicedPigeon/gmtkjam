﻿using System.Collections;
using System.Collections.Generic;
using DefaultNamespace;
using UnityEngine;

namespace Cards
{
    public class DrawCard : Card
    {
        protected override Vector3 GetPlayTargetPos()
        {
            return FindObjectOfType<EnemyAnimation>().transform.position;
        }

        protected override void Play()
        {
            CardSystem cardSystem = FindObjectOfType<CardSystem>();
            while (cardSystem.TryDrawCard())
            {
            }
        }
    }
}
