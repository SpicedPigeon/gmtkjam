using DefaultNamespace;
using Zenject;

namespace Cards
{
    public class SwiftStrikeCard : StrikeCard
    {
        public int DamageCap = 2;
        private SignalBus _signalBus;

        [Inject]
        public void InjectMethod(SignalBus signalBus)
        {
            _signalBus = signalBus;
            _signalBus.Subscribe<PostActionSignal>(OnBeat);
        }

        private void OnBeat()
        {
            if(Damage > DamageCap)
                Damage -= 1;
        }

        protected override void Play()
        {
            _signalBus.Unsubscribe<PostActionSignal>(OnBeat);
            base.Play();
        }
    }
}