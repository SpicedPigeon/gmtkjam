using System;
using System.Collections;
using Interaction;
using UnityEngine;
using Time = UnityEngine.Time;

namespace Cards
{
    [RequireComponent(typeof(HoveringInteraction))]
    public abstract class Card : MonoBehaviour
    {
        [SerializeField]
        private Animator _animator;

        [NonSerialized]
        public Card ParentPrefab;

        private static readonly int Draw = Animator.StringToHash("Draw");
        private static readonly int PlayAnim = Animator.StringToHash("Play");

        public void PlayDrawCardAnimation()
        {
            _animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
            _animator.SetTrigger(Draw);
            StartCoroutine(DrawAnimation());
        }

        private IEnumerator DrawAnimation()
        {
            const float timeAnimation = 0.1f;
            float timeLeft = timeAnimation;

            Vector3 startPos = transform.localPosition;
            
            while (timeLeft >= 0)
            {
                transform.localPosition = Vector3.Lerp(startPos, Vector3.zero, 1 - timeLeft / timeAnimation);
                yield return null;
                timeLeft -= Time.deltaTime;
            }

            transform.localPosition = Vector3.zero;
            _animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
        }
        
        public void PlayCard()
        {
            FindObjectOfType<CardSystem>().PlayCard(this);

            Play();
            
            //Destroy(gameObject);
            PlayCardAnimation();
        }
        
        public void PlayCardAnimation()
        {
            _animator.cullingMode = AnimatorCullingMode.CullUpdateTransforms;
            _animator.SetTrigger(PlayAnim);
            StartCoroutine(PlayAnimation());
        }

        private IEnumerator PlayAnimation()
        {
            const float timeAnimation = 0.3f;
            float timeLeft = timeAnimation;

            
            Vector3 startPos = transform.position;
            Vector3 targetPos = GetPlayTargetPos();

            Quaternion targetRot = Quaternion.Euler(0,0, Vector2.SignedAngle(Vector2.up, targetPos - startPos));

            while (timeLeft >= 0)
            {
                transform.position = Vector3.Lerp(startPos, targetPos, 1 - timeLeft / timeAnimation);
                transform.rotation = Quaternion.Lerp(Quaternion.identity, targetRot, 1 - timeLeft / timeAnimation);
                yield return null;
                timeLeft -= Time.deltaTime;
            }

            transform.position = targetPos;
            _animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            
            Destroy(gameObject);
        }

        protected abstract Vector3 GetPlayTargetPos();

        protected abstract void Play();

        public string Description;
    }
}