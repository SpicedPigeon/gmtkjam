using DefaultNamespace;
using DefaultNamespace.Health;
using DefaultNamespace.Player;
using UnityEngine;
using Zenject;

namespace Cards
{
    public class CounterCard : Card
    {
        [Inject]
        private HealthHolder _healthHolder;
        
        [Inject]
        private StatusEffects _statusEffects;

        [Inject]
        private CounterCardCountingInstance _counterCardCounting;

        protected override Vector3 GetPlayTargetPos()
        {
            return FindObjectOfType<EnemyAnimation>().transform.position;
        }

        protected override void Play()
        {
            if (_healthHolder.PlayerHealth.DamagedLastBeat != 0)
            {
                _counterCardCounting.NumberOfCountersSuccessfull++;
                int damage = _statusEffects.GetDamageModified(_healthHolder.PlayerHealth.DamagedLastBeat * _counterCardCounting.NumberOfCountersSuccessfull);
                
                _healthHolder.EnemyPlayerHealth.Damage(damage);
                
                Debug.Log("Countered damage: " + damage);
            }
        }
    }
}