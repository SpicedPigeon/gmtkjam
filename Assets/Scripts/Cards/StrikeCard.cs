using System;
using DefaultNamespace;
using DefaultNamespace.Health;
using DefaultNamespace.Player;
using TMPro;
using UnityEngine;
using Zenject;

namespace Cards
{
    public class StrikeCard : Card
    {
        [SerializeField]
        private int _damage;

        [SerializeField]
        private TMP_Text _damageText;
        
        [Inject]
        private StatusEffects _statusEffects;

        public int Damage
        {
            get => _damage;
            set
            {
                value = Math.Max(0, value);
                _damage = value;
                _damageText.SetText(_damage.ToString());
            }
        }

        private void Update()
        {
            _damageText.SetText(_statusEffects.GetDamageModifiedPreview(_damage).ToString());
        }

        protected override Vector3 GetPlayTargetPos()
        {
            return FindObjectOfType<EnemyAnimation>().transform.position;
        }

        protected override void Play()
        {
            FindObjectOfType<HealthHolder>().EnemyPlayerHealth.Damage(_statusEffects.GetDamageModified(Damage));
        }
    }
}