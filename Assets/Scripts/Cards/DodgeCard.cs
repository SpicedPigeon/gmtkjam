using DefaultNamespace;
using DefaultNamespace.Player;
using UnityEngine;
using Zenject;

namespace Cards
{
    public class DodgeCard : Card
    {
        [Inject]
        private StatusEffects _statusEffects;

        [Inject]
        private DiContainer _container;

        protected override Vector3 GetPlayTargetPos()
        {
            return FindObjectOfType<EnemyAnimation>().transform.position;
        }

        protected override void Play()
        {
            _statusEffects.CurrentEffects.Add(_container.Instantiate<DodgeEffect>());
        }
    }
}